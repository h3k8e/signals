# SIGNALS

SIGNALS is a minimalist blog design that is structured around clean lines, geometric shapes, and stark shades meant to mimic brutalist architecture. It is currently a concept at present, but is intended for conversion to a full working blog theme for applications like GravCMS.